#!/bin/bash

chown -R mysql:mysql /var/lib/mysql

mysql_install_db --user=mysql > /dev/null

/usr/sbin/mysqld --user mysql --bootstrap << SQL
    FLUSH PRIVILEGES;
    GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
SQL

/usr/bin/mysqld_safe --user mysql

